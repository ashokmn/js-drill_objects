function invert(obj) {
    let newObj = {};
    for (let values in obj) {
        newObj[obj[values]] = values;
    }
    return newObj;
}

module.exports = invert;
