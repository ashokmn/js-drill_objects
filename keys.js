function keys(obj) {
    let array = [];
    for (let key in obj) {
        array.push(key);
    }
    return array;
}

module.exports = keys;
