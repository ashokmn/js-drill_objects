function mapObject(obj, cb) {
    let array = [];
    for (let values in obj) {
        let newNumber = cb(obj[values]);
        array.push(newNumber);
    }
    return array;
}

module.exports = mapObject;
