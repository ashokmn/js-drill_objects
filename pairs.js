function pairs(obj) {
    let array = [];
    for (let values in obj) {
        let keyValue = [];
        keyValue.push(values);
        keyValue.push(obj[values]);
        array.push(keyValue);
    }
    return array;
}

module.exports = pairs;
