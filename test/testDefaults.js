const defaults = require('../defaults')

const defaultProps = { name: 'Ashok', age: 25, location: 'Davanagere', company: 'Mountblue' };

const testObject = { name: 'Bruce Wayne', age: 36, location: undefined };

const expected = { name: 'Bruce Wayne', age: 36, location: 'Davanagere', company: 'Mountblue' };

const actual = defaults(testObject,defaultProps);

if (JSON.stringify(actual) === JSON.stringify(expected)) {
    console.log('Test Passed');
} else {
    console.log('Test Failed');
}
