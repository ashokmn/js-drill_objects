const invert = require('../invert')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const expected = { '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' };

const actual = invert(testObject);

if (JSON.stringify(actual) === JSON.stringify(expected)) {
    console.log('Test Passed');
} else {
    console.log('Test Failed');
}
