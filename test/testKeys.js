const keys = require('../keys');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const expected = ['name', 'age', 'location'];

const actual = keys(testObject);

if (JSON.stringify(actual) === JSON.stringify(expected)) {
    console.log('Test Passed');
} else {
    console.log('Test Failed');
}
