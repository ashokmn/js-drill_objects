const mapObject = require('../mapObject')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function double(element) {
    if (typeof element === 'string') {
        return element + ' ' + element;
    } else {
        return element * 2;
    }

}

const expected = ['Bruce Wayne Bruce Wayne', 72, 'Gotham Gotham'];

const actual = mapObject(testObject, double);

if (JSON.stringify(actual) === JSON.stringify(expected)) {
    console.log('Test Passed');
} else {
    console.log('Test Failed');
}
