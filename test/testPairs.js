const pairs = require('../pairs')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const expected = [['name', 'Bruce Wayne'], ['age', 36], ['location', 'Gotham']];

const actual = pairs(testObject);

if (JSON.stringify(actual) === JSON.stringify(expected)) {
    console.log('Test Passed');
} else {
    console.log('Test Failed');
}
