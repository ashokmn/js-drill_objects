const values = require('../values');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const expected = ['Bruce Wayne', 36, 'Gotham'];

const actual = values(testObject);

if (JSON.stringify(actual) === JSON.stringify(expected)) {
    console.log('Test Passed');
} else {
    console.log('Test Failed');
}
